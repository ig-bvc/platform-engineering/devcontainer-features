#!/usr/bin/env bash
set -e

package_name="devcontainer"
package_version="${VERSION:-"0.65.0"}"

npm install -g @devcontainers/cli@${package_version}
